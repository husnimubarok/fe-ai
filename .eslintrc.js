module.exports = {
 root: true,
 env: {
  node: true
 },
 extends: [
  'eslint:recommended',
  'plugin:vue/recommended',
  "plugin:vue/recommended",
  'plugin:vue/essential',
  '@vue/standard'
 ],
 parserOptions: {
  parser: 'babel-eslint'
 },
 rules: {
  "vue/max-attributes-per-line": "off",
  "no-unused-vars": "off",
  "space-before-function-paren": "off",
  "comma-dangle": "off",
  indent: "off",
  'space-before-function-paren': ['error', {
   anonymous: 'never',
   named: 'never',
   asyncArrow: 'never'
  }],
  "comma-dangle": ["error", {
   arrays: "never",
   objects: "never",
   imports: "never",
   exports: "never",
   functions: "never"
  }],
  'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
  'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
  quotes: 'off'
 }
}
