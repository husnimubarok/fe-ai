const payloadBalikanSearch = {
  score: '',
  filename: '',
  no_permohonan: '',
  kelas: '',
  status: '',
  judul_invensi: ''
}

const imgSearchReturnPayload = {
  status: '',
  judul_invensi: '',
  
}

const contohPayloadSearch = {
  fileName: 'image-1635130555971.jpg',
  no_permohonan: 'A00201902844',
  text: 'kata pencarian',
  kelas: ['semua', '13-03', '01-02'],
  status: [1, 5],
  tglPermohonan: ['25-10-2021', '30-11-2021'],
  tglTerdaftar: ['25-10-2021', '30-11-2021'],
  tglExpired: ['25-10-2021', '30-11-2021'],
  logic: 'OR || AND'
}

const payloadMerek = {
	"fileName": "image-1634897124610.png",
	"applicationId": "D002002014741",
	"text": "PisangGorengMadu Bu Nanik Sejak 2007",
	"kelas": [
    "semua",
		"Class_30",
		"Class_43"
	],
	"status": [1,5],
	"tglPermohonan": [
		"30-11-2012",
		"30-11-2022"
	],
	"tglTerdaftar": [
		"30-11-2012",
		"30-11-2022"
	],
	"tglExpired": [
		null,
		null
	],
	"logic": "or",
  "toleransi":75
}


const patenPayload = {
	"text": "",	
	"kelas": [],
	"status": [],
	"tglPermohonan": [],
	"tglTerdaftar": [],
	"tglExpired": [],
	"logic": "and",
  "toleransi":75,
	"filters":[
		{
			"field": "Title",
			"value": "ladle",
			"operator": "="
		},
		{
			"field": "Abstract",
			"value": "lelehan",
			"operator": "=",
			"logic": "and"
		},
		{
			"field": "Claim",
			"value": "logam",
			"operator": "=",
			"logic": "and"
		},
		{
			"field": "Filing Date",
			"value": ["14-01-2014", "15-11-2015"],
			"operator": "=",
			"logic": "and"
		},
		{
			"field": "Publication Date",
			"value": ["14-01-2014", "15-11-2015"],
			"operator": "=",
			"logic": "and"
		},
		{
			"field": "Status",
			"value": [1, 2],
			"operator": "=",
			"logic": "and"
		},
		{
			"field": "Class",
			"value": ["B41J 5/00"],
			"operator": "=",
			"logic": "and"
		}
	]
}

merek - uraian barang
        and or

DI - prespektif done
     upload gambar stretch done
     and or done

