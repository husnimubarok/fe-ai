import axios from 'axios'

console.log('php local variable', process.env.VUE_APP_PHP_API)
console.log('node local variable', process.env.VUE_APP_NODE_API)
console.log('python local variable', process.env.VUE_APP_PYTHON_API)
console.log('php local variable DEMO', process.env.VUE_APP_PHP_APIDEMO)

const client = axios.create({
  // baseURL: 'http://103.150.169.71:8483',
  baseURL: process.env.VUE_APP_PHP_API,
  json: true
})

const client2 = axios.create({
  // baseURL: 'http://localhost:3003',
  // baseURL: 'http://103.150.169.71:3003',
  baseURL: process.env.VUE_APP_NODE_API,
  json: true
})

const client4 = axios.create({
  // baseURL: 'http://localhost:3003',
  // baseURL: 'http://103.150.169.71:3003',
  baseURL: 'http://10.2.7.47:3003',
  json: true
})

const client3 = axios.create({
  baseURL: process.env.VUE_APP_PYTHON_API,
  json: true
})

export default {
  async executePHP(method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  async executeNODE(method, resource, data) {
    return client2({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  async executePHYTON(method, resource, data) {
    return client3({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  async executeNODE2(method, resource, data) {
    return client4({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  getDesignClass() { // get class for searcing
    return this.executePHP('get', `${process.env.VUE_APP_PHP_APIDEMO}/php/oracle_get_class_di.php`)
  },
  getDataByText(kelas, text) { // search by word
    console.log('phonetic search', kelas, text)
    return this.executeNODE('GET', `/phonetic_di_old/${kelas}/${text}`)
    // return axios({
    //   method: 'GET',
    //   url: `http://103.150.169.71:3003/phonetic_di/${kelas}/${text}`
    // })
  },
  getDataByImageDI(filename) { // search by image uploaded
    return this.executePHYTON('POST', `/indexdesain/${filename}`)
    // return axios({
    //   method: 'GET',
    //   url: `http://103.150.169.71:5001/indexdesain/${filename}`
    // })
  },
  uploadDIImage(uploadImages) { // for upload image
    console.log('uploadImages', ...uploadImages)
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/desain_industri/upload_img`,
      // url: 'http://localhost:3003/ai/desain_industri/upload_img',
      data: uploadImages,
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: uploadEvent => {
        console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100) + '%')
      }
    })
  },
  newApi(payload) {
    return axios({
      method: 'POST',
      url: 'http://103.150.169.71:3003/ai/desain_industri/search',
      data: payload
    })
    /*
      contoh payload
      {
        imagePath: file path,
        option: 'des_in',
        text: 'text',
        kelas: 'kelas',
        status: 'status',
        tanggal: '10-10-2020'
      }
    */
  },
  mixSearchDI(kelas, text, filename, payload) {
    // http://103.150.169.71:3003/mix_search_di/semua/TELEPON GENGGAM/image-1634897124610.png
    return axios({
      method: 'POST',
      // url: `http://serverapi:3003/mix_search_di/${kelas}/${text}/${filename}`,
      url: 'http://103.150.169.71:3003/mix_search_di/semua/TELEPONGENGGAM/image-1634897124610.png',
      data: payload
    })
    /*
      contoh payload
      {
        no_permohonan: 'A00202000088',
        status: [1, 5],
        tglPermohonan: [03-09-2019, 03-10-2019],
        tglTerdaftar: [],
        tglExpired: []
      }
    */
  },
  search(data) {
    // console.log('search')
    return this.executeNODE2('post', 'desain_industri/search', data)
    // return this.executeNODE('post', '/desain_industri/search2', data)
    // return axios({
    //   method: 'POST',
    //   // url: `http://serverapi:3003/mix_search_di/${kelas}/${text}/${filename}`,
    //   url: 'http://10.2.7.47:3003/desain_industri/search',
    //   data: data
    // })
  }
}
