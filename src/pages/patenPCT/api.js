import axios from 'axios'

export default {
  submitPCT(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/universal/paten/process`,
      // url: `http://localhost:3003/ai/merek/search`,
      data: payload
    })
  }
}
