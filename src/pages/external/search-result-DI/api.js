import axios from 'axios'

export default {
  saveBandingData(payload) {
    return axios({
      method: 'POST',
      url: 'http://103.150.169.71:3003/ai/desain_industri/save_selected_search_result',
      data: payload
    })
  }
}
