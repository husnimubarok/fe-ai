import axios from 'axios'

export default {
  submitPCT(payload) {
    console.log(`${process.env.VUE_APP_NODE_API}/ai/universal/paten/process2`)
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/universal/paten/process2`,
      // url: `http://localhost:3003/ai/merek/search`,
      data: payload
    })
  }
}
