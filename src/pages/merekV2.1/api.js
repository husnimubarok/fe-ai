import axios from 'axios'

export default {
  uploadMerekImage(uploadImages) { // for upload image
    console.log('uploadImages', ...uploadImages)
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/desain_industri/upload_img`,
      // url: 'http://localhost:3003/ai/desain_industri/upload_img',
      data: uploadImages,
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: uploadEvent => {
        console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100) + '%')
      }
    })
  },
  getMerekClass() {
    return axios({
      method: 'GET',
      url: `${process.env.VUE_APP_PHP_API}/php/oracle_get_class.php`
    })
  },
  searchMerekPhonetic(text, kelas) {
    return axios({
      method: 'GET',
      url: `${process.env.VUE_APP_PHP_API}/php/oracle_phonetic.php?text=${text}&kelas=${kelas}`
    })
  },
  getDataByImageMerek(filename) {
    // ${process.env.VUE_APP_PYTHON_API}/indexung/dXBsb2FkLzE2MzUxNTM4NjRfQTAwMjAyMDAwODQ3XzU1MzBfVGFtcGFrU2FtcGluZ0tpcmkuanBn/merek
    return axios({
      method: 'GET',
      url: `${process.env.VUE_APP_PYTHON_API}/indexmerek/${filename}`
    })
  },
  search(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/search`,
      // url: `http://localhost:3003/ai/merek/search`,
      data: payload
    })
  },
  searchV2(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/searchV2`,
      // url: `http://localhost:3003/ai/merek/search`,
      data: payload
    })
  },
  searchV21(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/searchV21`,
      // url: `http://localhost:3003/ai/merek/search`,
      data: payload
    })
  }
}
