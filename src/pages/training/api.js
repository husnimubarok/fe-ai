
import axios from 'axios'

export default {
  uploadZip(uploadImages) { // for upload image
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/training_concept/upload_file`,
      data: uploadImages,
      headers: { 'Content-Type': 'multipart/form-data' },
      onUploadProgress: uploadEvent => {
        console.log('Upload Progress: ' + Math.round(uploadEvent.loaded / uploadEvent.total * 100) + '%')
      }
    })
  },
  submitCategory(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/upload_file_zip`,
      data: payload
    })
  },
  trainModel() {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/trainingModel`
    })
  },
  getAllCategory() {
    return axios({
      method: 'GET',
      url: `http://10.20.14.11:5001/getArrayModel`
    })
  },
  postEnhanceForm(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/indexingByUser`,
      data: payload
    })
  },
  getEnhancedImages(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/searchES`,
      data: payload
    })
  },
  updateCatMerek(payload) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_NODE_API}/ai/merek/updateCatMerek`,
      data: payload
    })
  }
}
