import axios from "axios"

const client = axios.create({
  baseURL: process.env.VUE_APP_PYTHON_API,
  json: true
})

const client2 = axios.create({
  baseURL: process.env.VUE_APP_PHP_API,
  json: true
})

const client3 = axios.create({
  baseURL: process.env.VUE_APP_NODE_API,
  // baseURL: 'http://localhost:3003', //DEV MODE
  json: true
})

export default {
  async execute(method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  getPatentData(params, logic) {
    return this.execute("get", "/search_text/" + params + `/${logic}`)
  },
  async execute2(method, resource, data) {
    return client2({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  getSavedSearchResults1() {
    return this.execute2("get", "/php/oracle_get_penelusuran.php")
  },
  getSavedSearchResults() {
    return this.execute3("get", "/ai/patent/getHasilPenelusuran")
  },
  getSavedDetail(params) {
    return this.execute2(
      "get",
      `${process.env.VUE_APP_PHP_APIDEMO}/php/oracle_get_hasil_penelusuran.php?getvalue=` + params
    )
  },
  postSelectedData(data) {
    return this.execute3('post', '/ai/patent/save_selected_search_result', data)
  },
  postSelectedData1(textInput, payload) {
    // return this.execute2('post', '/php/submit_checklist.php?text_input=' + textInput, payload) //PHP
    return this.execute2('post', '/ai/patent/save_selected_search_result')
  },
  async execute3(method, resource, data) {
    return client3({
      method,
      url: resource,
      data
    }).then(res => {
      return res.data
    })
  },
  // search (payload) {
  // return this.execute3('POST', '/ai/paten/search', payload)
  // },
  search(payload) {
    return this.execute3('POST', '/paten/find?document=djki', payload)
  },
  async getDocFilePatent() {
    try {
      // console.log("API getting doc for Patent...")
      const res = await axios({
        method: "GET",
        url: `${process.env.VUE_APP_NODE_API}/paten/getOfficeDocPenelusuranPatent/`,
        // url: "http://localhost:3003/desain_industri/getOfficeDocPenelusuranPatent/",
        data: this.data,
        responseType: "blob"
      })
      const blob = new Blob([res.data], {
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      })
      const url = window.URL.createObjectURL(blob)
      window.open(url)
      // console.log("response", res)
    } catch (err) {
      console.log("err", err)
    }
  },
  async getDocxFilePatent(row, headTitle) {
    try {
      // console.log("API getting doc for Patent...")
      // const listRow = JSON.stringify(row)
      // const listRow = JSON.parse(row).split("|")
      const res = await axios({
        method: "GET",
        // url: `${process.env.VUE_APP_NODE_API}/desain_industri/getOfficeDocPenelusuranPatent/`,
        url: `${process.env.VUE_APP_NODE_API}/paten/getOfficeDocxPenelusuranPatent/${row}/${headTitle}`,
        data: this.data,
        responseType: "blob"
      })
      const blob = new Blob([res.data], {
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
      })
      const url = window.URL.createObjectURL(blob)
      window.open(url)
      console.log("response")
    } catch (err) {
      console.log("err", err)
    }
  }
}
