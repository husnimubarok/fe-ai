import axios from 'axios'

const client3 = axios.create({
 baseURL: process.env.VUE_APP_NODE_API_NEW,
 json: true
})

// const doc = 'paten'
const doc = 'djki'

export default {
 async execute3(method, resource, data) {
  return client3({
   method,
   url: resource,
   data
  }).then(res => {
   return res.data
  })
 },
 search(payload) {
  return this.execute3('POST', `/test_elastic/filter?doc=${doc}`, payload)
 }
}
