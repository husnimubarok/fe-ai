import axios from 'axios'

export default {
 search_no_image(payload) {
  return axios({
   method: 'POST',
   // url: `http://localhost:3000/test_elastic/filter_di`,
   url: `${process.env.VUE_APP_NODE_API_NEW}/test_elastic/filter_di`,
   data: payload
  })
 },
 search_by_image(payload) {
  return axios({
   method: 'POST',
   url: `${process.env.VUE_APP_NODE_API_NEW}/test_elastic/filter_di_image`,
   data: payload
  })
 },
 search(payload) {
  return axios({
   method: 'POST',
   url: `${process.env.VUE_APP_PYTHON_API_NEW}/find_di`,
   data: payload
  })
 },
}
