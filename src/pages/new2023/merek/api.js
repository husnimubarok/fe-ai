import axios from 'axios'

export default {
 getMerekClass() {
  return axios({
   method: 'GET',
   url: `${process.env.VUE_APP_PHP_API}/php/oracle_get_class.php`
  })
 },
 search_no_image(payload) {
  return axios({
   method: 'POST',
   url: `${process.env.VUE_APP_NODE_API_NEW}/test_elastic/filter_merek`,
   data: payload
  })
 },
 search_by_image(payload) {
  return axios({
   method: 'POST',
   url: `${process.env.VUE_APP_NODE_API_NEW}/merek/test`,
   data: payload
  })
 },
 search(payload) {
  return axios({
   method: 'POST',
   url: `${process.env.VUE_APP_PYTHON_API_NEW}/find_merek`,
   data: payload
  })
 },
}
