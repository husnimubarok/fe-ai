import Vue from 'vue'
import VueRouter from 'vue-router'
// import MainPage from '../pages/mainPage.vue'
import Paten from '../pages/paten/paten.vue'
import Merek from '../pages/merek/merek.vue'
import DesainIndustri from '../pages/desain-industri/desainIndustri.vue'
import UploadExternal from '../pages/uploadExternal/uploadExternal.vue'
import patenPCT from '../pages/patenPCT/patenPCT.vue'
import patenPCTV2 from '../pages/patenPCTV2/patenPCTV2.vue'

// const Merek = () => import('../pages/merek/merek.vue')
// const NewMerek = () => import('../pages/merek/newMerek.vue')
// const DesainIndustri = () => import('../pages/desain-industri/desainIndustri.vue')

// V2
import PatenV2 from '../pages/patenV2/patenV2.vue'
import MerekV2 from '../pages/merekV2/merekV2.vue'

// Merek V2.1
import MerekV21 from '../pages/merekV2.1/merekV2.1.vue'

// Merek V3
import MerekV3 from '../pages/merekV3/merekV3.vue'

// Merek V4
import MerekV4 from '../pages/merekV4/merekV4.vue'
import MerekV41 from '../pages/merekV4.1/merekV4.1.vue'

// Training Screen
import TrainingConcept from '../pages/training/index.vue'

// NEW 2023
import NewMerek from '../pages/new2023/merek/merek.vue'
import NewPaten from '../pages/new2023/paten/paten.vue'
import NewDi from '../pages/new2023/di/di.vue'
// NEW 2023

// External
const MerekExternal = () => import('../pages/external/merek/merek.vue')
const DesainIndustriExternal = () => import('../pages/external/desain-industri/desainIndustri.vue')
const PatenExternal = () => import('../pages/external/paten/paten.vue')

Vue.use(VueRouter)

const routes = [
 {
  path: '/',
  name: 'mainPage',
  redirect: '/new-paten'
  // redirect: '/paten'
 },
 {
  path: '/merek',
  name: 'merek-search-result',
  component: Merek
 },
 {
  path: '/merek-search-v2',
  name: 'merek-search-result-v2',
  component: MerekV2
 },
 {
  path: '/merek-search-v2.1',
  name: 'merek-search-result-v2.1',
  component: MerekV21
 },
 {
  path: '/merek-search-v3',
  name: 'merek-search-result-v3',
  component: MerekV3
 },
 {
  path: '/merek-search-v4',
  name: 'merek-search-result-v4',
  component: MerekV4
 },
 {
  path: '/merek-search-v4.1',
  name: 'merek-search-result-v4.1',
  component: MerekV41
 },
 {
  path: '/desain-industri',
  name: 'desain-industri-search-result',
  component: DesainIndustri
 },
 {
  path: '/paten',
  name: 'Paten',
  component: Paten
 },
 {
  path: '/paten-v2',
  name: 'Paten V2',
  component: PatenV2
 },
 {
  path: '/upload-external',
  name: 'uploadExternal',
  component: UploadExternal
 },
 {
  path: '/paten-pct',
  name: 'paten-pct',
  component: patenPCT
 },
 {
  path: '/paten-pct-v2',
  name: 'paten-pct-v2',
  component: patenPCTV2
 },
 {
  path: '/external',
  name: 'External',
  redirect: '/external/merek-search'
 },
 {
  path: '/external/merek-search',
  name: 'merek-search-external',
  component: MerekExternal
 },
 {
  path: '/external/desain-industri-search',
  name: 'desain-industri-search-external',
  component: DesainIndustriExternal
 },
 {
  path: '/external/paten',
  name: 'Paten-external',
  component: PatenExternal
 },
 {
  path: '/training-concept',
  name: 'Training Concept',
  component: TrainingConcept
 },
 {
  path: '/new-merek',
  name: 'new-merek',
  component: NewMerek
 },
 {
  path: '/new-paten',
  name: 'new-paten',
  component: NewPaten
 },
 {
  path: '/new-di',
  name: 'new-di',
  component: NewDi
 }
]

const router = new VueRouter({
 mode: 'history',
 base: process.env.BASE_URL,
 routes
})

export default router
